import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const posts = new PostsController();
const chai = require('chai');

describe(`Posts controller`, () => {
    let userId: number;    
    let postId: number;  
    let userAccessToken: string;
 
    it(`should return 200 status code and get all posts`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response,200);
        checkResponseTime(response,2000);   
    });

    it(`should return 200 status code and login`, async () => {
        let response = await users.authUser("mohikancem@gmail.com", "qwerty");

        checkStatusCode(response,200);
        checkResponseTime(response,1000);

        userId = response.body.user.id;              
        userAccessToken = response.body.token.accessToken.token;
    });

    it(`should return 200 status code and create a post`, async () => {
        let response = await posts.createPost(userId, userAccessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,2000);

        postId = response.body.id;
    });

    it(`should return 200 status code and like post`, async () => {
        let response = await posts.likePost(userId, userAccessToken, postId);

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    it(`should return 200 status code and comment post`, async () => {
        let response = await posts.commentPost(userId, userAccessToken, postId, "Hello");

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    it(`should return 200 status code and get all posts`, async () => {
        let response = await posts.getAllPosts();

        checkStatusCode(response,200);
        checkResponseTime(response,2000);   
    });

    it(`should return a 401 status code and not allow a post to be created without authorization`, async () => {
        userAccessToken = "";
        let response = await posts.createPost(userId, userAccessToken);

        checkStatusCode(response,401);
        checkResponseTime(response,2000);

        postId = response.body.id;
    });

    it(`should return 401 status code and not allow liking a post without authorization`, async () => {
        userAccessToken = "";
        let response = await posts.likePost(userId, userAccessToken, postId);

        checkStatusCode(response,401);
        checkResponseTime(response,1000);
    });

    it(`should return 401 status code and not allow commenting a post without authorization`, async () => {
        userAccessToken = "";
        let response = await posts.commentPost(userId, userAccessToken, postId, "Hello");

        checkStatusCode(response,401);
        checkResponseTime(response,1000);
    });

});
