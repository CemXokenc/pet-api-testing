import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const chai = require('chai');

describe(`Users controller`, () => {
    let userId: number;    
    let userEmail: string;  
    let userAccessToken: string;
 
    before(`should return 201 status code and confirm that the user is registered`, async () => {
        let response = await users.registerUser();

        checkStatusCode(response,201);
        checkResponseTime(response,1000);   
    });

    it(`should return 200 status code and list all users`, async () => {
        let response = await users.getAllUsers();        

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    before(`should return 200 status code and login`, async () => {
        let response = await users.authUser("mohikancem@gmail.com", "qwerty");

        checkStatusCode(response,200);
        checkResponseTime(response,1000);

        userId = response.body.user.id;        
        userEmail = response.body.user.email;        
        userAccessToken = response.body.token.accessToken.token;
    });

    it(`should return 200 status code and get detailed information about current user`, async () => {
        let response = await users.getUsersFromToken(userAccessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    it(`should return 204 status code and update the current user's information`, async () => {
        let response = await users.updateUser(userAccessToken, userId);

        console.log(userId);
        checkStatusCode(response,204);
        checkResponseTime(response,1000);
    });

    it(`should return 200 status code and get detailed information about current user`, async () => {
        let response = await users.getUsersFromToken(userAccessToken);

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    it(`should return 200 status code and get detailed information about current user`, async () => {
        let response = await users.getUsersById(userId);

        checkStatusCode(response,200);
        checkResponseTime(response,1000);
    });

    after(`should return 204 status code and delete current user`, async () => {
        let response = await users.deleteUser(userAccessToken, userId);

        checkStatusCode(response,204);
        checkResponseTime(response,1000);
    });
});
