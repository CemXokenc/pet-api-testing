import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const chai = require('chai');

describe(`A specific test data set`, () => {
    let invalidCredentialsDataSet = [
        { email: 'mohikancem@gmail.com', password: 'qwerty', code: 200 },
        { email: 'mohikancem@gmail.com', password: ' ', code: 401 },
        { email: 'invalid@gmail.com', password: 'qwerty', code: 404 },
        { email: 'invalid@gmail.com', password: ' ', code: 404 }
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await users.authUser(credentials.email, credentials.password);

            checkStatusCode(response,credentials.code);
            checkResponseTime(response,1000);
        });
    });

});
