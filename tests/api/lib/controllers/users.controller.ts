import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class UsersController {
    async registerUser() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body({
                "avatar": "avatar.png",
                "email": "mohikancem@gmail.com",
                "userName": "CemmiXokenc",
                "password": "qwerty"
            })
            .send();
        return response;
    }
    
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;
    }

    async authUser(email: string, password: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Auth/login`)
            .body({
                "email": email,
                "password": password
            })
            .send();
        return response;
    }

    async getUsersFromToken(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async updateUser(accessToken: string, userId: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("PUT")
            .url(`api/Users`)
            .body({
                "id": userId,
                "avatar": "avatar.jpg",
                "email": "mohikanket@gmail.com",
                "userName": "KettiXokenc"
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async getUsersById(userId: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Users/` + userId)
            .send();
        return response;
    }

    async deleteUser(accessToken: string, userId: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("DELETE")
            .url(`api/Users/` + userId)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}