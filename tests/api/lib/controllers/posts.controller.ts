import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {        
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(userId: number, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts`)
            .body({
                "authorId": userId,
                "previewImage": "string",
                "body": "string"                
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async likePost(userId: number, accessToken: string, postId: number) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Posts/like`)
            .body({
                "entityId": postId,
                "isLike": true,
                "userId": userId
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async commentPost(userId: number, accessToken: string, postId: number, postBody: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Comments`)
            .body({
                "authorId": userId,
                "postId": postId,
                "body": postBody
            })
            .bearerToken(accessToken)
            .send();
        return response;
    }
    
}